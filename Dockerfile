FROM openjdk:8-jdk-alpine
MAINTAINER ravdeep.singh.techie@gmail.com
WORKDIR /opt/app
COPY target/user-service*.jar users-service.jar
ENTRYPOINT ["java","-jar","users-service.jar"]
