
# User-Service
This a micro-service for managing users

### Prerequisites

Java
Maven

##### Build 
```
mvn clean install
```

##### Run
```
mvn spring-boot:run -Dspring-boot.run.profiles=local
```

## Sample request

  - Import postman collection and environment from postman directory  
 

## Deployment

Docker 

## Built With
* [Maven](https://maven.apache.org/) - Dependency Management


## Authors

* **Ravdeep Singh** - *Initial work* - [User-Service](https://gitlab.com/ravdeep-singh/user-service)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
