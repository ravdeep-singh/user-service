package org.rsn.services.user.service.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.rsn.services.user.service.api.dto.AddressDto;
import org.rsn.services.user.service.api.dto.UserDto;
import org.rsn.services.user.service.app.UserServiceConfiguration;
import org.rsn.services.user.service.core.user.User;
import org.rsn.services.user.service.core.user.UserHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.rsn.services.user.service.common.UserMapper.USER_MAPPER;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ContextConfiguration(classes = {UserServiceConfiguration.class})
@WebMvcTest(value = UserController.class, excludeAutoConfiguration = {SecurityAutoConfiguration.class})
@Import(UserController.class)
public class UserControllerTest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private UserHandler handler;

    @MockBean
    private UserServiceConfiguration userServiceConfiguration;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    @DisplayName("GetUserByValidIdIsSuccessful")
    public void givenUserIdWhenGetUserThenReturnUserDetailsJson()
            throws Exception {
        final UserDto userDto = getUserDto(getAddressDto());
        final User user = USER_MAPPER.dtoToDomain(userDto);
        given(handler.findById(1)).willReturn(user);
        mvc.perform(get("/users/{id}", userDto.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstname", is(userDto.getFirstName())));
    }

    @Test
    @DisplayName("GetAllUserIsSuccessful")
    public void givenAllExistingUser()
            throws Exception {

        given(handler.findAll()).willReturn(getUsers());
        mvc.perform(get("/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)));
    }

    @Test
    @DisplayName("CreateUserIsSuccessful")
    public void createUserThenReturnUserDetailsJson()
            throws Exception {
        final UserDto userDto = getUserDto(getAddressDto());
        final User user = USER_MAPPER.dtoToDomain(userDto);
        given(handler.save(user)).willReturn(user);
        mvc.perform(post("/users")
                .content(objectMapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    @DisplayName("UpdateUserIsSuccessful")
    public void updateUserThenReturnUserDetailsJson()
            throws Exception {
        final UserDto userDto = getUserDto(getAddressDto());
        final User user = USER_MAPPER.dtoToDomain(userDto);
        given(handler.findById(1)).willReturn(user);
        user.setFirstName("Updated");
        given(handler.save(user)).willReturn(user);
        mvc.perform(put("/users/{id}", userDto.getId())
                .content(objectMapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    @DisplayName("DeleteUserIsSuccessful")
    public void deleteUser()
            throws Exception {
        final UserDto userDto = getUserDto(getAddressDto());

        mvc.perform(delete("/users/{id}", userDto.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
        verify(handler).deleteById(userDto.getId());
    }

    private UserDto getUserDto(AddressDto addressDto) {
        return new UserDto(1, "Mr", "Ravdeep", "Singh", "Male", addressDto);
    }

    private List<UserDto> getUsersDtoList(AddressDto addressDto) {
        return Arrays.asList(new UserDto(1, "Mr", "Ravdeep", "Singh", "Male", addressDto),
                new UserDto(2, "Miss", "Sansa", "Stark", "Female", addressDto),
                UserDto.builder()
                        .id(3)
                        .title("Mr")
                        .firstName("Jon")
                        .lastName("Snow")
                        .gender("Male")
                        .addressDto(addressDto)
                        .build());
    }

    private List<User> getUsers() {
        return getUsersDtoList(getAddressDto())
                .stream()
                .map(USER_MAPPER::dtoToDomain)
                .collect(Collectors.toList());
    }

    private AddressDto getAddressDto() {
        return AddressDto.builder()
                .city("Sydney")
                .pincode(3122)
                .state("NSW")
                .street("Albert")
                .build();
    }
}
