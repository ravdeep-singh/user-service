package org.rsn.services.user.service.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;


@EnableJpaRepositories(basePackages = {"org.rsn.services.user.service.repository"})
@EntityScan("org.rsn.services.user.service.repository")
@SpringBootApplication(scanBasePackages = {"org.rsn.services.user.service.api",
        "org.rsn.services.user.service.app",
        "org.rsn.services.user.service.core",
        "org.rsn.services.user.service.common"})
public class UserService {
    public static void main(String[] args) {
        SpringApplication.run(UserService.class, args);
    }
}
