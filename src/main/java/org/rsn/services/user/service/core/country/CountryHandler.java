package org.rsn.services.user.service.core.country;

import lombok.extern.slf4j.Slf4j;
import org.rsn.services.user.service.common.CountryNotFoundException;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Slf4j
@Service
public class CountryHandler {

    private final RestTemplate restTemplate;

    public CountryHandler() {
        this.restTemplate = new RestTemplate();
    }

    public String getCountry(String countryCode) {

        log.info("Getting country details for country code:{}", countryCode);

        final Country country = restTemplate
                .exchange("https://restcountries.com/v2/alpha/{countryCode}"
                        , HttpMethod.GET
                        , null
                        , new ParameterizedTypeReference<Country>() {
                        }, countryCode).getBody();

        log.info("Response received country:{}", country);
        if (Objects.isNull(country) || Objects.isNull(country.getName())) {
            throw new CountryNotFoundException();
        }
        return country.getName();
    }

}
