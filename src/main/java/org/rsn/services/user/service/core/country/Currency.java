package org.rsn.services.user.service.core.country;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Currency {
    private String code;
    private String name;
    private String symbol;
}
