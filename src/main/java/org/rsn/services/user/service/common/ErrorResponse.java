package org.rsn.services.user.service.common;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {
    private final String errorMessage;
    private final String errorDetails;

    public ErrorResponse(String message, String details) {
        super();
        this.errorMessage = message;
        this.errorDetails = details;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getErrorDetails() {
        return errorDetails;
    }
}
