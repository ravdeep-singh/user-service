package org.rsn.services.user.service.core.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("WeakerAccess")
public class User {

    private Integer id;

    private String title;

    private String firstName;

    private String lastName;

    private String gender;

    private Address address;
}
