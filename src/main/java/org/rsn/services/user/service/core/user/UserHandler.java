package org.rsn.services.user.service.core.user;

import org.rsn.services.user.service.common.UserNotFoundException;
import org.rsn.services.user.service.core.country.CountryHandler;
import org.rsn.services.user.service.repository.AddressPo;
import org.rsn.services.user.service.repository.UserPo;
import org.rsn.services.user.service.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.shuffle;
import static org.rsn.services.user.service.common.UserMapper.USER_MAPPER;

@Service
@SuppressWarnings({"UnusedDeclaration"})
public class UserHandler {

    private static final List<String> COUNTRY_CODES = Arrays.asList("AUS", "IND");

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CountryHandler countryHandler;

    public User findById(Integer id) {
        final Optional<UserPo> userPO = userRepository.findById(id);

        if (!userPO.isPresent()) {
            throw new UserNotFoundException();
        }

        return USER_MAPPER.poToDomain(userPO.get());
    }

    public Collection<User> findAll() {
        return userRepository.findAll()
                .stream()
                .map(USER_MAPPER::poToDomain)
                .collect(Collectors.toList());

    }

    public void deleteById(Integer id) {
        userRepository.deleteById(id);
    }

    public User save(User user) {
        final Address address = user.getAddress();
        final String country = Objects.nonNull(address) ? countryHandler.getCountry(address.getCountryCode()) :"NA";
        final UserPo userPo = USER_MAPPER.domainToPO(user);
        final AddressPo addressPo = userPo.getAddressPo();
        addressPo.setCountry(country);
        final UserPo savedUserPO = userRepository.save(userPo);
        return USER_MAPPER.poToDomain(savedUserPO);
    }
}
