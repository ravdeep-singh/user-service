package org.rsn.services.user.service.api;

import lombok.extern.slf4j.Slf4j;
import org.rsn.services.user.service.api.dto.UserDto;
import org.rsn.services.user.service.core.user.User;
import org.rsn.services.user.service.core.user.UserHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.rsn.services.user.service.common.UserMapper.USER_MAPPER;

@RestController()
@Validated
@Slf4j
public class UserController {

    @Autowired
    private UserHandler userHandler;

    @GetMapping(value = "/users/{id}", produces = "application/json")
    public UserDto getUser(@Valid @NotNull @Positive @PathVariable Integer id) {

        return USER_MAPPER.domainToDto(userHandler.findById(id));
    }

    @GetMapping(path = "/users", produces = "application/json")
    public List<UserDto> getUsers() {

        return userHandler.findAll()
                .stream()
                .map(USER_MAPPER::domainToDto)
                .collect(Collectors.toList());
    }

    @DeleteMapping(value = "/users/{id}", produces = "application/json")
    public void deleteUser(@Valid @NotNull @Positive @PathVariable Integer id) {
        userHandler.deleteById(id);
    }

    @PostMapping("/users")
    public ResponseEntity<UserDto> createUser(@RequestBody UserDto userDto) {
        User user = USER_MAPPER.dtoToDomain(userDto);
        User savedUser = userHandler.save(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(USER_MAPPER.domainToDto(savedUser));
    }

    @PutMapping(value = "/users/{id}")
    public ResponseEntity<UserDto> updateUser(@RequestBody UserDto userDto, @PathVariable Integer id) {
        final User user = userHandler.findById(id);
        if (Objects.isNull(user))
            return ResponseEntity.notFound().build();
        userDto.setId(id);
        User updatedUser = userHandler.save(USER_MAPPER.dtoToDomain(userDto));
        return ResponseEntity.ok(USER_MAPPER.domainToDto(updatedUser));
    }
}

