package org.rsn.services.user.service.common;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.rsn.services.user.service.api.dto.UserDto;
import org.rsn.services.user.service.core.user.User;
import org.rsn.services.user.service.repository.UserPo;

import static org.mapstruct.factory.Mappers.getMapper;

@Mapper
public interface UserMapper {

    UserMapper USER_MAPPER = getMapper(UserMapper.class);

    @Mapping(source="addressDto", target="address")
    User dtoToDomain(final UserDto userDto);

    @Mapping(source="address", target="addressDto")
    UserDto domainToDto(final User user);

    @Mapping(source="address", target="addressPo")
    UserPo domainToPO(final User user);

    @Mapping(source="addressPo", target="address")
    User poToDomain(final UserPo userPo);
}
