package org.rsn.services.user.service.repository;

import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "users")
@ToString
public class UserPo {

    private Integer id;

    private String title;

    private String firstName;

    private String lastName;

    private String gender;

    private AddressPo addressPo;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "title", nullable = false)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "first_name", nullable = false)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "last_name", nullable = false)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "gender", nullable = false)
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    public AddressPo getAddressPo() {
        return addressPo;
    }

    public void setAddressPo(AddressPo addressPo) {
        this.addressPo = addressPo;
    }

}
