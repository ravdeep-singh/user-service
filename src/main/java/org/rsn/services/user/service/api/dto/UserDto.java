package org.rsn.services.user.service.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {

    private Integer id;

    @NotNull
    private String title;

    @NotNull
    @JsonProperty("firstname")
    private String firstName;

    @NotNull
    @JsonProperty("lastname")
    private String lastName;

    @NotNull
    private String gender;

    @NotNull
    @JsonProperty("address")
    private AddressDto addressDto;

}
