package org.rsn.services.user.service.core.country;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Country{

    @NotNull
    private String name;
    private List<String> callingCodes;
    private String capital;
    private List<Currency> currencies;

}




