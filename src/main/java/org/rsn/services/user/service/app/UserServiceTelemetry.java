package org.rsn.services.user.service.app;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
@SuppressWarnings("unused")
public class UserServiceTelemetry {

    @Around("execution(* org.rsn.services.user.service.api..*(..))")
    public Object logBeforeAndAfterServiceMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info("Execution started - {}", joinPoint.getSignature());
        final Object result = joinPoint.proceed();
        log.info("Execution finished - {}", joinPoint.getSignature());
        return result;
    }
}
